---
id: configurando-ambiente
title: Configurando-Ambiente
sidebar_label: Configurando Ambiente
---

## Checkout projeto

Clonar o projeto do git, preferencialmente não clone direto pelo eclipse, faça isso via linha de comando ou pelo gitkraken. O importante é não importar o projeto no eclipse ainda, devido ele criar uns arquivos de configuração ocultos, precisamos usar os que estão no eclipse_config.

No exemplo a seguir farei por linha de comandos.

**Clone**
```git
git clone http://mportaldesenv.dynalias.com:8088/multiportal/rastreamento4x.git
```

navege até o diretório eclipse_config dentro do projeto

```sheel
cd /rastreamento4x/eclipse_config
```

execute o script eclipse_config.sh, esse script moverá os arquivos `context.xml` e `app.conf` e alguns arquivos ocultos de configuração do próprio eclipse.

```shell
./eclipse_config.sh
```

o resultado do script deve ser este

```sheel
**Iniciando Configuração do projeto**
Movendo arquivos de configuração do eclipse para diretório raiz do projeto
Copiando .classpath para diretório raiz
Copiando .project para diretório raiz
Copiando .tern-project diretório raiz
Copiando diretório .settings para diretório raiz
Copiando app.conf para diretório /src/main
Copiando context.xml para diretório /WebContent/META-INF
**Arquivos foram copiados para os diretórios**
**Fim Configuração do projeto ;)**
```

Agora vamos para o eclipse

## Importando no eclipse

Abra o eclipse e clique com direito e selecione a opção import

![Captura_de_Tela_2019-05-10_às_13.44.00](/uploads/bbef1f139607bb7872cb34428209b06b/Captura_de_Tela_2019-05-10_às_13.44.00.png)

Selecione a opção 'Existing projects into workspace'

![Captura_de_Tela_2019-05-10_às_13.44.18](/uploads/58549e6a2f69ea930ad268d42945c4dc/Captura_de_Tela_2019-05-10_às_13.44.18.png)

clique no botão browse e abra a pasta do projeto, ao abrir pode ser que ele liste mais de um projeto, deixe marcado somente a opção rastreamento4x conforme a imagem abaixo. **Obs: se importar o projeto `eclipse_config` bem provavelmente vai dar pau**

![Captura_de_Tela_2019-05-10_às_13.48.06](/uploads/2261e0d189d5b2cb2eabe0ee72e4c0f8/Captura_de_Tela_2019-05-10_às_13.48.06.png)

Após importar o projeto ele virá cheio de erros, vamos dar um jeito nisso.

### Configurar JRE

Clique com botão direito do mouse sobre o projeto, e selecione a opção Build Path > Configure Build Path

![Captura_de_Tela_2019-05-10_às_14.13.08](/uploads/3816a4f7f59964d60fb23bbde9494d7f/Captura_de_Tela_2019-05-10_às_14.13.08.png)

Selecione a aba Libraries e edite seu JRE que provavelmente deve estar apontando erro

![Captura_de_Tela_2019-05-10_às_14.20.33](/uploads/43a43b1d828743e1ed98d9b0230581f0/Captura_de_Tela_2019-05-10_às_14.20.33.png)

![Captura_de_Tela_2019-05-10_às_14.20.50](/uploads/2b181edf07ef9443c7694bb250e46078/Captura_de_Tela_2019-05-10_às_14.20.50.png)

Selecione todas as libs e clique em remover

![Captura_de_Tela_2019-05-10_às_14.22.14](/uploads/b2f52dea91ce5b58bd317dcc2ca782fe/Captura_de_Tela_2019-05-10_às_14.22.14.png)

Clique em `apply`

Selecione a opção `Validation`, habilite a opção `Enable project specific settings` e habilite, após isso desabilite as opções de validação de XML, `XML Schema Validator` e `XML Validator`

![Captura_de_Tela_2019-05-13_às_10.43.14](/uploads/479dc24c85bed66b73d96161b075c2ac/Captura_de_Tela_2019-05-13_às_10.43.14.png)

clique em `apply`

Navegue no projeto até o diretório `WebContent > WEB-INF > lib`, selecione todas as libs, clique com direito e clique em add build path

![Captura_de_Tela_2019-05-10_às_14.43.38](/uploads/ec4daaab9a842542f0c9745ed8f8a91a/Captura_de_Tela_2019-05-10_às_14.43.38.png)

projeto continua apresentando erros? Relaxa que isso é normal, precisamos configura o tomcat ainda, então vamos lá

### Configurar Tomcat

Primeiramente baixe o (apache tomcat 7)[https://tomcat.apache.org/download-70.cgi]

Vamos lá, no eclipse, caso não tenha a aba servers, clique em `window > show view > pesquise por servers`

![Captura_de_Tela_2019-05-10_às_14.48.19](/uploads/f288abfadfea0c67b4c9f341441502be/Captura_de_Tela_2019-05-10_às_14.48.19.png)

![Captura_de_Tela_2019-05-10_às_14.48.30](/uploads/fc069025fdea1ca217fe7e7475beaed2/Captura_de_Tela_2019-05-10_às_14.48.30.png)

Clique para adicionar um novo servidor, e selecione a opção tomcat 7 e clique
em avançar.

![Captura_de_Tela_2019-05-10_às_14.48.38](/uploads/67dab42109a88ab0129cae83942289f6/Captura_de_Tela_2019-05-10_às_14.48.38.png)

![Captura_de_Tela_2019-05-10_às_14.49.31](/uploads/522580131a6250b870ec3cf641b2cbbd/Captura_de_Tela_2019-05-10_às_14.49.31.png)

Aponte o server que fez download

![Captura_de_Tela_2019-05-10_às_15.19.48](/uploads/52ad3e1dbb95225c1ef739110b7c4271/Captura_de_Tela_2019-05-10_às_15.19.48.png)

Clique com direito sobre o servidor e selecione a opção Add and Remove

![Captura_de_Tela_2019-05-10_às_16.08.34](/uploads/015cd79d30b3a5c585afd470181afcce/Captura_de_Tela_2019-05-10_às_16.08.34.png)

adicione o projeto rastreamento

![Captura_de_Tela_2019-05-10_às_16.08.51](/uploads/9a18f58236933a709ace2e51597e9b50/Captura_de_Tela_2019-05-10_às_16.08.51.png)

Dê dois cliques no servidor, agora alteraremos alguns parametrôs de configuração do tomcat

na aba Timeouts aumentaremos o tempo de stop e start, pois o projeto é grande e não consegue ser startado com valor default que vem no eclipse de 45s

Aumentaremos ambos parametros start em 450s e stop 150s

![Captura_de_Tela_2019-05-13_às_10.58.36](/uploads/61aafdb2c8f1ea690ccb01f5c8b93dbc/Captura_de_Tela_2019-05-13_às_10.58.36.png)

agora abriremos a opção Open launch configuration

![Captura_de_Tela_2019-05-13_às_10.58.56](/uploads/1ffb2eeef0623747ed88907f2d4053be/Captura_de_Tela_2019-05-13_às_10.58.56.png)

selecione a aba `arguments`, cole os seguintes parametros no campo `VM Arguments`

`-server -Xms768m -Xmx1024m -XX:MaxPermSize=256m`

![Captura_de_Tela_2019-05-10_às_17.39.34](/uploads/f4d1e1bf0518ac7a1bf5004d285ca2cc/Captura_de_Tela_2019-05-10_às_17.39.34.png)

start o servidor e após iniciá-lo acesse o localhost e verifiquei se o projeto subiu

![Captura_de_Tela_2019-05-13_às_11.02.26](/uploads/45a0c72e008214e50ab0e9894c4b4e3f/Captura_de_Tela_2019-05-13_às_11.02.26.png)

No browser acesse

http://localhost:8080/tracksys/

![Captura_de_Tela_2019-05-13_às_14.10.30](/uploads/a3b8253e272397f1dc10f2cc33b38203/Captura_de_Tela_2019-05-13_às_14.10.30.png)

# Prontinho, agora bora programaaaar!!